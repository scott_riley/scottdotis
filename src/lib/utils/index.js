import snarkdown from 'snarkdown'

export const fetchMarkdownPosts = async (limit = null) => {
  const allPostFiles = import.meta.glob('/src/routes/writing/about/*.md')
  const iterablePostFiles = Object.entries(allPostFiles)
  
  const allPosts = await Promise.all(
    iterablePostFiles.map(async ([path, resolver]) => {
      const { metadata } = await resolver()
      const postPath = path.slice(11, -3)

      return {
        meta: metadata,
        path: postPath,
      }
    })
  )
  if(limit) {
    return allPosts.slice(0, limit)
  }
  return allPosts
}

export const snarkdownEnhanced = (input) => {
  return input
    .split(/(?:\r?\n){2,}/)
    .map((l) =>
      [" ", "\t", "#", "-", "*", ">"].some((char) => l.startsWith(char))
        ? snarkdown(l)
        : `<p>${snarkdown(l)}</p>`
    )
    .join("\n")
}