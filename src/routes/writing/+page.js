export const load = async ({ fetch }) => {
  const response = await fetch(`/api/posts`)
  const posts = await response.json()

  const featuredPost = posts.find(item => {
    return item.meta.category === 'featured'
  })

  return {
    featuredPost,
    posts
  }
}