import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export async function load({ params }){
  const post = await prisma.microPost.findUnique({
    where: {
      slug: params.slug
    }
  })
  return post
}