import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export const load = async () => {
  const posts = await prisma.microPost.findMany({
    orderBy: [
      { createdAt: 'desc' }
    ]
  })

  return {
    posts
  }
}