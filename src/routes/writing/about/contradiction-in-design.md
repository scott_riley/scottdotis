---
title: "Embracing contradiction in design"
date: "2020-04-29"
---

<script>
  import FaceHint from '../../../components/FaceHint.svelte'
</script>

In tech, we love neat problems. As an ‘X’, I want to ‘Y’, so that I can ‘Z’. Fuckin sorted. Answer enough of this rhetoric and you have a feature set. Ship it.

We love neat problems because neat problems are solvable, testable, measurable problems. Extrapolated to a problem space, we love neat problem spaces because neat problem spaces represent repeatable, provable, simulatable behaviours.

I hate neat problems. I hate neat problems because neat problems are homogenous, often exclusive, sometimes oppressive. I hate neat problem spaces because neat problem spaces represent repeatable, provable, simulatable behaviours.

When we artificially (and we do, we can’t help ourselves) constrain our problem spaces, we often do so to remove contradiction. That contradiction might come from an ethnographic interview that suggests a key feature of our planned product is undesirable, can be used for harm. That contradiction might come from usability studies that suggest certain people have difficulty using our shit in a distracted environment.

## Contradicting Assumptions

An important point of research is to challenge inherent assumptions and biases found in the teams responsible for speccing, designing, building and testing our products. Challenging assumptions creates a cognitive dissonance where those who hold said assumptions must explore just where that bias comes from. Oftentimes it comes from a place of honest, innocent ignorance, like how a person with perfect vision might assume a 12px body font size is ever okay. Other times it comes from places of unconscious systemic racism, homophobia, misogyny, transphobia or class discrimination. Forgetting things like how the cognitive burden of poverty is equivalent to a 13-point drop in effective IQ, or that gender identity is not binary, or that hate speech is a thing that should maybe be policed on your platform Jack you fuckin dickhead.

Anyway. When we learn something that challenges our assumptions—depending how deeply-ingrained those assumptions are in our internal schema for the world—things get tricky for us. Essentially, we form schemas and mental models of our world. Our schemas give us ways of defining categories. Like how we might understand some _thing_ to be a bird if said thing is covered in feathers and has a beak and flaps a lot. Or how our schema for music might include _diatonic melodies and harmonies_ (an inherently western notion of music). Most of the time these definitions form ‘boundaries’ for what we may or may not see as a member of a specific category.

People with immovable boundaries in their schematic understanding of the world are people we might call ‘snobs’ (if the subject is jazz music, perhaps) or ‘abhorrent fucking bigots’ (if the subject is, I dunno, so-called gender-critical feminism). People whose schematic boundaries are a bit more fluid are generally open-minded, able to change and adapt their mental models, and statistically less likely to own a neckbeard.

When there is a suggestion that our schemas are incomplete, misleading, or just plain wrong, then the dissonance we experience is likely to be relative to the mutability of our schema. If we’re just learning about a subject, and we’re relatively open-minded, new information is likely to simply pique our interest. If we have a long-held, or deep-set belief, then we’re far more likely to be flustered, disbelieving, or to feel outright attacked. Never argue with a bigot, as the saying goes, mostly because they’re dickheads, but also because their schemas are often set in stone.

So, contradiction in one sense, revolves around the ever-important task of keeping (often-privileged, sometimes homogenous) product teams‘ feet on the ground. If our research presents myriad challenges to assumptions, we’re probably doing good research. If we notice ways people are using our product that go against how we expected (and designed) our products to be interacted with, that’s another insightful contradiction.

When this happens, open-minded product teams will likely get excited; it’s a chance to sculpt a product to cover more use cases, to build in a whole new sense of understanding into future features. Closed-minded product teams might simply see this contradiction as a series of edge-cases, or if they’re really big fuckheads, blame the participant for daring to use a product in an ‘incorrect’ way.

## Contradicting Problems

Another role of contradiction in our process lies in ensuring that our research, documentation, and problem space definitions are as reflective of the real world as we can muster. This means that we throw away neat problems and we accept that we’re going to encounter problems that contradict each other.

The biggest manifestation of _not_ doing this that I notice is clients who only present a single persona, empathy map or storyboard as their research culmination.

Trying to aggregate your research participants into a single archetype is an exercise in erasure and assumption-gathering. It’s great in that it makes my life easier as The Dickhead Who Has To Tell People Their Work Is Biased, but it’s a recipe for limited, homogenous products.

If you only have one persona, presenting a contradiction is almost impossible. Yes, as humans, we contradict ourselves all the time—we say one thing but act differently, for example, and documenting differences between stated behaviour and observed behaviour is a hugely critical part of research. However, when we distill our findings down into an archetype such as a persona, it can get very difficult to continue this nuance.

Things get way more interesting when you work with multiple Personas. Things get way more interesting than that when you make sure your Personas contradict one another.

When we understand that the world is full of contradiction, that two people in the same problem space could have vastly different primary problems, and that those differences and expectations will be brought to any session with our product, the need for that contradiction to be represented in our documentation and research is hopefully clear.

The same goes for Mental Models, if you’re documenting them. And Empathy Maps, and Storyboards, and whatever the fuck else you do as you document your early stage problem space explorations. Do multiple versions, make sure they provide friction amongst themselves.

Very briefly, let’s roll with the following definitions:

*   A Persona is an archetype of a _person_
*   A mental model explains their _beliefs and expectations_
*   An empathy map is an archetype of a _situation_ they might find themselves in

I like to follow the following rule: every persona should have a mental model and at least one empathy map. Pretty standard stuff; but add in the previous rule of requiring multiple personas, with at least a little contradiction between their needs, motivations, beliefs and expectations, and you set up the basis of a framework that can present a much more diverse problem space.

As a third rule, I try to make sure that every persona can also challenge an ingrained assumption that’s present in a product team. We’re fallible human beings and we work on prediction, categorisation and schemata; we’re _bound_ to have our assumptions and biases. By ensuring Personas challenge some of these assumptions, along with the previous two rules, we’re able to create a documentation framework that presents both problem-level contradiction (different desires, ideas and beliefs that people will bring to our products) and assumption-level contradiction (challenging the internal biases of a product team).

## Give me an example you insufferable tit

So this is all heavy on the why, but not necessarily on the how. Namely because it’s difficult and you could easily write [two whole chapters of a book](https://mindfuldesign.xyz) (well would you look the fuck at that) on it, but also because it’s all quite situational and dependent.

Some examples I’ve worked into real-life personas:

*   Living close to the poverty line (the aforementioned cognitive burden and sensitivity to cost that many people in our industry are completely protected from)
*   Suffering with anxiety (unable to give as much focus as an able-minded, twenty-something tech bro who thinks 40 notifications a day is okay)
*   Changed their name after transitioning (the negative emotional impact of deadnaming is something transgender folk are all too familiar with)

There’s a certain uncomfortable vulgarity to documenting things like mental health issues, financial problems, oppression and exclusion. There’s also a deluge of ethical considerations that _must_ be made in the presentation of this data. While our research should be as diverse and as encompassing as we can muster, documenting information around sexuality, gender identity, race, religion and many other highly personal considerations comes with a whole host of important regulation and even more-important ethical requirements. If you’re unsure how to approach this with tact and compassion, you should 100% be hiring an expert to conduct your research.

Finally, in terms of challenging the assumptions of your team, this is something that can only really come about from interacting with each other and creating an open framework where people feel as though they can raise and discuss these issues without offending or misappropriating a colleague.

When it comes to contradiction within your problem space; it’s all about ensuring that you’re creating the right amount of archetypes. As mentioned before, distilling your whole research findings down into a single Persona is an exercise in erasure. A better approach would be to have multiple, more succinct Personas.

Let’s say we’re doing some research around a budget-setting and financial-planning app. Persona A might be extremely detail-focussed and want to see granular bits of information around all their spending habits, right down to the penny. Persona B, then, could be a lot less concerned with these finer details and just want to know the cliffnotes: how much can they spend now? How long before they run out of cash for the month? Persona A might be able to put a solid 10-15 minutes into a session with our app on their commute home from work. Persona B might have 30 seconds between their kid throwing yoghurt all down themselves and the dog knocking over a lamp.

Two personas; two very different sets of needs, expectations, situations and tolerances for bullshit.

This is, of course, an extremely basic, stark example. You’ll likely be dealing with more subtlety and much more variance. The point is to embrace contradiction, to ensure that your problems are messy, lived through differently by different people. To understand that the environment you design in is so far removed from the environment within which your product is going to be interacted with.

## Problems aren’t neat

It’s kinda why they’re fucking problems in the first place. When we look for neat problems, we start with a tacit willingness to let assumption and erasure permeate our process at the ground floor. Messy, contradictory, demanding problem spaces give us a platform to explore the fallibility of people and their conflicting desires. When we do this, we expose ourselves to the power structures that might have us unconsciously placing some groups’ needs above others’. We can have open discussions about the blind spots and biases that our teams hold. We can document our research in ways that make it reflective of the sociopolitical trash fire that is the real world right now.

I don’t think that _all_ tech companies out there prefer neat problems. I also think that there are so many times and places where a succinct problem definition is completely invaluable. But building a product around the misguided notion that enough people experience a particular problem, perceive its challenges in a particular way, and want the same outcome from a session with its purported solution is naive and dangerous. Seeing the world as this kind of meritocratic, logical canvas—where any product can be an absolute positive—is, to put it bluntly, centrist absurdity.

By embracing contradiction, both in terms of challenging internal assumptions and presenting a valid facsimile of the external world in our documentation, we can attempt to skirt the blind positivity that grips the tech industry right now—where VC’s can fund away at their whim, where convenience services are valued in the multiple billions of dollar ranges, where we pay thousands of employees hundreds of thousands of dollars a year each to rinse every last drop out of attention economies. We’re bombarded with the notion that design solves problems and that companies that solve these perennial problems can exist and prosper. I think it’s time that we started holding these problems—and subsequently their ‘solvers’—to a higher, more realistic standard.