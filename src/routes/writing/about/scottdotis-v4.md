---
title: "Scott.is v4: embracing the personal site renaissance"
date: "2022-12-22"
---

<script>
  import FaceHint from '../../../components/FaceHint.svelte'
</script>

Welcome to the fourth iteration of my Very Cool And Cozy Personal On Line Web Presence™. This is the biggest overhaul I’ve done to my shit in a _long_ time. Especially seeing as the last version was literally me shutting everything down.

I’m going to talk a little about the motiviations for this build and the technology powering the site – especially because it's the first time in almost 15 years that I’ve built a site without the express purpose of using it as a means to attract clients and find work.

## Personal sites are a playground

Like, you have this _thing_ – and it’s _yours_! You don’t have KPIs to hit (unless you set them yourself in which case fucking hell get a grip). You don’t have an overbearing boss pushing you in a direction you disagree with. You don’t have to work to a ‘stack’ you hate or use words you don’t agree with. You can just fuckin shout at your computer and nice shit comes out the other side.

I’m using this site (which I guess is technically a server-side rendered web app) to experiment with all kinds of things. Because I’m using Svelte & SvelteKit, it’s so fucking easy for me to just build something. 

![A microblog](/microblog-tutorial.png)

I [built a microblog](/writing/about/building-a-microblog-with-svelte-planetscale-and-prisma) in a few hours. It’s fun as fuck and I think you should build one too. If I want to add webmentions to blog posts it’ll probably take me an hour or two. If I wanted to make some freakish fucking members area where you pay $11.42 and get access to me narrating my favourite Shrek scenes I could probably do that too.

<p class="big">It’s empowering as fuck to know you have somewhere to just fuck around and build weird shit.</p>

<FaceHint text="I even have this absolutely fucking stupid FaceHint component that I can throw into any blog post. Do I have this on Twitter? On Medium? I think the fuck not." />

## Own your shit
A huge motivation for going all-in on a rebuild is that I want to be prepared for the big backlash that I expect will happen against centralised social media platforms. There‘s absolutely already a clamour to move away from Twitter now that Apartheid Jetson and his insufferable reply guys have stomped it into the ground. I don’t expect many more Web 2.0 social/social-adjacent platforms will survive the tides changing.

I want this version of my site to be more of a cozy corner of the web that I own, control, and have as much agency as possible over.

### Write your way out
I fucking love blogs. I love making them, I love reading them, I love everything from the most basic “I’m blogging cause I think I have to” gaffs, to the weird fanfiction shit, to the huge Smashing Magazine-style monoliths, and everything in between.

I also love writing, and I’m now almost fully recovered from the absolute brain melt that was writing a book. I want to get back to sharing knowledge, ideas, and 1,800-word shitposts.

## Speak for yourself
Homogenous social media designs and hyper-popular play-it-safe web templates have made the web feel stagnant and repetitive. I _want_ my stuff to live somewhere that speaks for me. I want to design and build shit from scratch, without using egregious templates or other peoples’ work. I don’t think you’re a dopey gobshite if you _do_ like homogeneity and templates, but nah, not for me pal.

You’ll notice parts of this site that are jank as fuck. You’ll probs be able to spot the stuff I didn’t give as much of a fuck about. There’ll be spacing issues and weird typography and other little details that aren’t quite right. I’m totally fine with that. My place needs tidying but I’d rather have a bit of clutter in a room that I love.

---

## Powered by…
If you give a fuck, here’s a bunch of the stuff that’s powering this site.

### Frontend: Svelte & SvelteKit
This site is built in [Svelte](https://svelte.dev/), and uses the Skeleton [SvelteKit](https://kit.svelte.dev/docs/introduction) starter project. When you read a post in my blog, you’re reading a markdown file that’s been parsed at build time by `mdsvex`.

(Okay so _technically_ you’re reading a markdown file that also `include`s some Svelte components when needed. Because `mdsvex` is cool like that and my whole site can just be a clusterfuck of mixed content.)

I _adore_ Svelte. It’s equal parts throwback and revolutionary. If you’re sick of idiosyncratic React shite, give it a whirl.

### Backend: PlanetScale & Prisma
[Tiny Words](/writing/tiny-things) – my microblog – uses [PlanetScale](https://planetscale.com) for the database and [Prisma](https://prisma.io) for the ORM. Any future dynamic content that isn’t being pulled from markdown files will also use this.

I’ve heard this combination described as ‘database as code’ – with the amazing Taylor Barnett giving one of the best breakdowns of this approach in a [Next.js Conf talk that’s up on YouTube](https://www.youtube.com/watch?v=5JpKZfPx-1k). I’m not exaggerating when I say learning the basics of these two platforms is one of the most empowering breakthroughs I’ve made in recent time.

### Design: Figma & Rogie’s Noise & Texture plug-in
As a ‘design in the browser’ person, if you saw how I use Figma you’d probably have a fucking aneurysm. It’s a shambles and I love it. One thing that is _not_ a shambles is <a href="https://rog.ie" target="_blank" rel="noreferrer">Rogie King</a>’s amazing [Noise & Texture plugin](https://www.figma.com/community/plugin/1138854718618193875/Noise-%26-Texture), which, as you can probably tell, has been given an extensive run-out across this site.

Noise is back fuckers. Embrace it.

<FaceHint text="Hi! Scott’s face again. If you want to poke around, the source code for this site is up on GitLab." cta="See the code" url="https://gitlab.com/scott_riley/scottdotis" external />

## Go forth and build weird shit
We’re in a pretty exciting time where there’s a genuine and concerted push away from mainstream social media and hypercapitalist eyes-on-pixel models. More people are going to be moved by weird shit that’s built with passion and craft.

Your personal site doesn’t have to be a dusty, boring old thing. It doesn’t have to be a glorified CV. It can be a fun as fuck playground, a place where you can express yourself more completely, and something that inspires curiosity in others.

If you need any help, giz a shout xxx