---
title: "Interview: Mindful Technology"
date: "2019-02-19"
---

<script>
  import FaceHint from '../../../components/FaceHint.svelte'
</script>

I had the absolute pleasure of being interviewed by the folks at Mindful Technology. They're a wonderful bunch.

You can [read the full interview](https://www.mindfultechnology.com/blog/2019/2/19/long-read-mindful-design) over on their blog, but here's some lil excerpts.

> In a society that is increasingly reliant upon tech solutions that appeal mostly to the white, affluent people who create said solutions, the role of design as a combatant and leveler is only going to become more and more important. By designing with vulnerability, cognitive impairment, worries, anxieties and intrinsic motivation in mind, we’re better equipped to serve real humans in an age of stark disparity

— Me, cause I'm so fuckin edgy

---

> In tech, especially in design, we pride ourselves on simplifying complex actions. Our whole field revolves around making things easier for people, and we’re actually really good at it. The question, though, is who do we simplify things for? What actions are we simplifying?

— Also Me, woah

---

> We have this kinda milquetoast middle ground where we try and ‘sprinkle’ fun into boring as heck apps, or we try and disingenuously tack political activism and ‘wokeness’ to the side of apps that are the embodiment of a safe bet. The world could do with more fun, more weirdness, more creativity-for-the-sake-of-creativity. Probably a lot more than it could do with another food delivery app.

— Me Again, the sheer insight of it all

<FaceHint text="Check out the interview if you wanna read more!" cta="Read it" url="https://www.mindfultechnology.com/blog/2019/2/19/long-read-mindful-design" external />
