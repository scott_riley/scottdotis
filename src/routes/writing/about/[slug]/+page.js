export async function load({ params }){
  const post = await import(`../${params.slug}.md`)
  const { title, date, og } = post.metadata
  const content = post.default

  return {
    og,
    content,
    title,
    date,
  }
}