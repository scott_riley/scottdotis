export const load = async ({ fetch }) => {
  const response = await fetch(`/api/posts`)
  let posts = await response.json()
  posts = posts.slice(0,6)

  return {
    posts
  }
}