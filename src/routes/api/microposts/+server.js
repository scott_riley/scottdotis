import { json } from '@sveltejs/kit'
import prisma from '../../../lib/prisma'

export const GET = async () => {
  const microposts = await prisma.microPost.findMany()
  return json(microposts)
}